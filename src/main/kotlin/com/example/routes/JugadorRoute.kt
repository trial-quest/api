package com.example.routes

import com.example.dao.daoJugador
import com.example.model.Usuario
import io.ktor.http.*
import io.ktor.server.application.*
import io.ktor.server.auth.*
import io.ktor.server.request.*
import io.ktor.server.response.*
import io.ktor.server.routing.*
import io.ktor.server.util.*
import java.io.File
import java.sql.Connection


fun Route.jugadorRoute() {
    authenticate("myAuth") {
        route("/jugador") {
            get {
                val jugadorList = daoJugador.allJugadores()
                if (jugadorList.isNotEmpty()) {
                    call.respond(jugadorList)
                } else {
                    call.respondText("No se han encontrado jugadores", status = HttpStatusCode.OK)
                }
            }
            get("({id?})") {
                val id = call.parameters["id"] ?: return@get call.respondText(
                    "Id de jugador incorrecta",
                    status = HttpStatusCode.BadRequest
                )
                val jugador = daoJugador.jugador(id.toInt()) ?: return@get call.respondText(
                    "No hay ningun jugador con la id $id",
                    status = HttpStatusCode.NotFound
                )
                call.respond(jugador)
            }
            get("/image/{imageName}") {
                val imageName = call.parameters["imageName"]
                var file = File("./src/main/resources/users/$imageName")
                if (file.exists()) {
                    call.respondFile(File("./src/main/resources/users/$imageName"))
                } else {
                    call.respondText("Image not found", status = HttpStatusCode.NotFound)
                }
            }
            post {
                val user = call.receive<Usuario>()
                daoJugador.addNewJugador(user.usu_username, user.usu_password)
                call.respondText("Usuario creado correctamente", status = HttpStatusCode.Created)
            }
            delete("({id?})") {
                val usu_id = call.parameters["usu_id"] ?: return@delete call.respond(HttpStatusCode.BadRequest)
                val jugador = daoJugador.jugador(usu_id.toInt()) ?: return@delete call.respondText(
                    "No hay ningun jugador con id $usu_id",
                    status = HttpStatusCode.NotFound
                )
                if (daoJugador.deleteJugador(usu_id.toInt())) {
                    call.respondText("Jugador borrado correctamente", status = HttpStatusCode.Accepted)
                }
            }
        }
    }
}

