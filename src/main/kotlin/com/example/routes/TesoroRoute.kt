package com.example.routes

import com.example.dao.dao
import com.example.dao.daoJugador
import com.example.dao.daoResena
import com.example.dao.daoTesoroUsuario
import com.example.model.Resena
import com.example.model.Tesoro
import com.example.model.TesoroUsuario
import com.google.gson.Gson
import io.ktor.http.*
import io.ktor.http.content.*
import io.ktor.server.application.*
import io.ktor.server.auth.*
import io.ktor.server.request.*
import io.ktor.server.response.*
import io.ktor.server.routing.*
import java.io.File
import java.time.LocalDate

fun Route.tesoroRoute() {
    authenticate("myAuth") {
        route("/tesoros") {
            //GET

            /*
            GET que obtiene todos los tesoros.
             */
            get {
                val tesoroList = dao.allTesoros()
                if (tesoroList.isNotEmpty()) {
                    call.respond(tesoroList)
                } else {
                    call.respondText("No se ha encontrado tesoros", status = HttpStatusCode.OK)
                }
            }

            /*
            GET que busca tesoro por nombre.
             */
            get("/{titulo?}") {
                val name = call.parameters["titulo"] ?: return@get call.respondText(
                    "Missing title",
                    status = HttpStatusCode.BadRequest
                )
                val tesoros = dao.allTesoros() ?: return@get call.respondText(
                    "No hay tesoros con el nombre $name",
                    status = HttpStatusCode.NotFound
                )
                val tesorosRespond = mutableListOf<Tesoro>()
                for (i in tesoros) {
                    if (name in i.titulo) {
                        tesorosRespond.add(i)
                    }
                }
                call.respond(tesorosRespond)
            }
            /*
            GET QUE BUSCA LOS TESOROS ENCONTRADOS DE UN USUARIO
            */
            get("/{user_id}/encontrados") {
                if (call.parameters["user_id"].isNullOrBlank()) {
                    return@get call.respondText("El id del usuario no es correcto.", status = HttpStatusCode.BadRequest)
                }
                val user_id = call.parameters["user_id"]?.toInt()
                val tesoroUsuarios = user_id?.let { it1 -> daoTesoroUsuario.tesorosEncontrados(it1) }
                if (tesoroUsuarios != null) {
                    if (tesoroUsuarios.isNotEmpty()) {
                        call.respond(tesoroUsuarios)
                    } else {
                        val emptylist = listOf<TesoroUsuario>()
                        call.respond(emptylist)
                    }
                }
            }

            /*
            GET QUE BUSCA LAS IMAGENES POR NOMBRE
            */


            get("/{tes_id}/resena") {
                if (call.parameters["tes_id"].isNullOrBlank()) {
                    return@get call.respondText("El id del tesoro no es correcto.", status = HttpStatusCode.BadRequest)
                }
                val tes_id = call.parameters["tes_id"]?.toInt()
                val resenasTesoro = daoResena.resenaById(tes_id!!.toInt())
                call.respond(resenasTesoro)
                if (resenasTesoro != null) {
                    call.respond(resenasTesoro)
                } else {
                    call.respondText("No se ha encontrado reseñas en  este tesoro", status = HttpStatusCode.OK)
                }
            }
            /*
            GET QUE AÑADE UNA IMAGEN A UNA RESEÑA
            */



            //POST
            /*
            POST QUE AÑADE UN TESORO
            */
            post {
                val data = call.receiveMultipart()
                var tesoro: Tesoro? = null
                var fileName = ""
                val gson = Gson()
                var titulo = ""
                var descripcion = ""
                var latitud = ""
                var longitud = ""

                data.forEachPart { part ->
                    when (part) {
                        is PartData.FormItem -> {
                            if (part.name == "tesoro_data") {
                                tesoro = gson.fromJson(part.value, Tesoro::class.java)
                            } else {
                                when (part.name) {
                                    "titulo" -> titulo = part.value
                                    "descripcion" -> descripcion = part.value
                                    "latitud" -> latitud = part.value
                                    "longitud" -> longitud = part.value
                                }


                            }

                        }

                        is PartData.FileItem -> {
                            fileName = part.originalFileName as String
                            var fileBytes = part.streamProvider().readBytes()
                            File("./src/main/resources/imagenes/$fileName").writeBytes(fileBytes)
                        }

                        else -> {}
                    }
                }
                tesoro = gson.fromJson(
                    """{"titulo":${titulo},"descripcion":${descripcion},"latitud":${latitud},"longitud":${longitud},"valoracion":"","foto":${fileName}}""",
                    Tesoro::class.java
                )


                val tesoroToPost = tesoro?.let { it1 ->
                    dao.addNewTesoro(
                        it1.titulo,
                        tesoro!!.descripcion,
                        tesoro!!.latitud,
                        tesoro!!.longitud,
                        tesoro!!.valoracion,
                        fileName
                    )
                }
                call.respondRedirect("/tesoros/${tesoroToPost?.id}")
            }


            /*
            POST QUE AÑADE UN TESORO ENCONTRADO DE UN USUARIO
            */
            post("/{user_id}/descubiertos/{tes_id}") {
                if (call.parameters["user_id"].isNullOrBlank()) {
                    return@post call.respondText(
                        "El id del usuario no es correcto.",
                        status = HttpStatusCode.BadRequest
                    )
                }
                println(LocalDate.now().toString())
                if (call.parameters["tes_id"].isNullOrBlank()) {
                    return@post call.respondText("El id del tesoro no es correcto.", status = HttpStatusCode.BadRequest)
                }
                val tes_id = call.parameters["tes_id"]?.toInt()
                val user_id = call.parameters["user_id"]?.toInt()
                daoTesoroUsuario.addTesoroEncontrado(true, LocalDate.now(), tes_id!!, user_id!!)
                call.respondText("Tesoro añadido a encontrados", status = HttpStatusCode.Created)
            }

            /*
            POST QUE AÑADE UNA RESEÑA A UN TESORO HECHA POR UN USUARIO
            */
            post("{user_id}/resena/{tes_id}") {
                if (call.parameters["tes_id"].isNullOrBlank()) {
                    return@post call.respondText("El id del tesoro no es correcto.", status = HttpStatusCode.BadRequest)
                }
                if (call.parameters["user_id"].isNullOrBlank()) {
                    return@post call.respondText(
                        "El id del usuario no es correcto.",
                        status = HttpStatusCode.BadRequest
                    )
                }
                val tes_id = call.parameters["tes_id"]?.toInt()
                val user_id = call.parameters["user_id"]?.toInt()
                val user = daoJugador.jugador(user_id!!)
                val data = call.receiveMultipart()
                var resena: Resena? = null
                var fileName = ""
                val gson = Gson()
                data.forEachPart { part ->
                    when (part) {
                        is PartData.FormItem -> {
                            if (part.name == "resena_data") {
                                resena = gson.fromJson(part.value, Resena::class.java)
                            }
                        }

                        is PartData.FileItem -> {
                            fileName = part.originalFileName as String
                            var fileBytes = part.streamProvider().readBytes()
                            File("./src/main/resources/resenas/$fileName").writeBytes(fileBytes)
                        }

                        else -> {}
                    }
                }
                daoResena.addResena(
                    resena!!.comentario,
                    resena!!.puntuacion,
                    fileName,
                    tes_id!!,
                    user!!.usu_id,
                    user.usu_username
                )
                call.respondText("Resena añadida correctamente", status = HttpStatusCode.Created)

            }
            put("/{tes_id}") {
                val id = call.parameters["tes_id"] ?: return@put call.respond(HttpStatusCode.BadRequest)
                val data = call.receiveMultipart()
                var tesoro: Tesoro?
                var fileName = ""
                val gson = Gson()
                var titulo = ""
                var descripcion = ""
                var latitud = ""
                var longitud = ""
                var foto = ""
                var fileUpdated = false
                data.forEachPart { part ->
                    when (part) {
                        is PartData.FormItem -> {
                            if (part.name == "tesoro_data") {
                                tesoro = gson.fromJson(part.value, Tesoro::class.java)
                            } else {
                                when (part.name) {
                                    "titulo" -> titulo = part.value.replace("'", "")
                                    "descripcion" -> descripcion = part.value.replace("'", "")
                                    "latitud" -> latitud = part.value
                                    "longitud" -> longitud = part.value
                                    "foto" -> foto = part.value.replace("'", "")
                                }
                            }

                        }

                        is PartData.FileItem -> {
                            fileName = part.originalFileName as String
                            fileUpdated = true
                            var fileBytes = part.streamProvider().readBytes()
                            File("./src/main/resources/imagenes/$fileName").writeBytes(fileBytes)
                        }

                        else -> {}
                    }
                }
                if (fileUpdated) {
                    tesoro =
                        Tesoro(id.toInt(), titulo, descripcion, latitud.toDouble(), longitud.toDouble(), "", fileName)
                } else {
                    tesoro = Tesoro(id.toInt(), titulo, descripcion, latitud.toDouble(), longitud.toDouble(), "", foto)
                }
                dao.editTesoro(
                    id.toInt(),
                    tesoro!!.titulo,
                    tesoro!!.descripcion,
                    tesoro!!.latitud,
                    tesoro!!.longitud,
                    "",
                    tesoro!!.foto_tesoro
                )
                call.respondText("Tesoro con id $id modificado correctamente.", status = HttpStatusCode.Accepted)
            }

            //DELETE
            /*
            DELETE QUE BORRA UN TESORO A PARTIR DE LA ID
            */
            delete("/{tes_id}") {
                val id = call.parameters["tes_id"] ?: return@delete call.respond(HttpStatusCode.BadRequest)
                dao.deleteTesoro(id.toInt())
                call.respondText("Tesoro eliminado", status = HttpStatusCode.Accepted)
            }
            delete("/{usu_id}/encontrados/{tes_id}") {
                val id = call.parameters["tes_id"] ?: return@delete call.respond(HttpStatusCode.BadRequest)
                val usu_id = call.parameters["usu_id"] ?: return@delete call.respond(HttpStatusCode.BadRequest)
                daoTesoroUsuario.deleteTesoro(id.toInt(), usu_id.toInt())
            }
        }
    }


    route("/tesoros") {

        get("/imagenes/{imageName}") {
            val imageName = call.parameters["imageName"]
            println(imageName)
            val file = File("./images/$imageName")
            println(file)
            if (file.exists()) {
                call.respondFile(File("./images/$imageName"))
            } else {
                call.respondText("Image not found", status = HttpStatusCode.NotFound)
            }
        }

        get("/resenas/{imageName}") {
            val imageName = call.parameters["imageName"]
            var file = File("./src/main/resources/resenas/$imageName")
            if (file.exists()) {
                call.respondFile(File("./src/main/resources/resenas/$imageName"))
            } else {
                call.respondText("Image not found", status = HttpStatusCode.NotFound)
            }
        }
    }
}




