package com.example.plugins

import com.example.model.Database
import com.example.routes.*
import io.ktor.server.routing.*
import io.ktor.server.application.*
import org.postgresql.util.PSQLException

fun Application.configureRouting() {
    routing {
        try {
            jugadorRoute()
            tesoroRoute()
            tesoroRouteWeb()
            jugadorRouteWeb()
            LoginRegister()
        }
        catch (e: PSQLException){
            println(e.message)
        }
    }
}
