package com.example.model

import kotlinx.serialization.Serializable
import org.jetbrains.exposed.sql.Table

@Serializable
data class Estadistica(
    val est_id: Int,
    val usu_id: Int,
    val usu_porcentaje: String,
    val usu_numTesorosEncontrados: Int
){

}

object Estadisticas: Table(){
    val est_id= integer("est_id").autoIncrement()
    val usu_id = integer("usu_id")
    val usu_porcentaje = varchar("usu_porcentaje",4)
    val usu_numTesorosEncontrados = integer("usu_numTesorosEncontrados")
    override val primaryKey = PrimaryKey(est_id)
}