package com.example.model

import com.example.dao.daoJugador
import io.ktor.server.auth.*
import java.security.MessageDigest
import kotlin.text.Charsets.UTF_8

data class DigestUserTable(val userName: String, val realm: String) : Principal

fun getMd5Digest(str: String): ByteArray = MessageDigest.getInstance("MD5").digest(str.toByteArray(UTF_8))

val myRealm = "Access to the '/' path"
var userTable: MutableMap<String, ByteArray> = mutableMapOf(
    "admin" to getMd5Digest("admin:$myRealm:password")
)

suspend fun uploadUser(): MutableMap<String, ByteArray> {
    val userList= daoJugador.allJugadores()
    for (i in userList){
        userTable.put(i.usu_username, getMd5Digest("${i.usu_username}:$myRealm:${i.usu_password}"))
    }
    return userTable
}
