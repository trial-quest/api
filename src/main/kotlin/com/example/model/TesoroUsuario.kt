package com.example.model

import kotlinx.serialization.Contextual
import kotlinx.serialization.KSerializer
import kotlinx.serialization.Serializable
import kotlinx.serialization.descriptors.PrimitiveKind
import kotlinx.serialization.descriptors.PrimitiveSerialDescriptor
import kotlinx.serialization.descriptors.SerialDescriptor
import kotlinx.serialization.encoding.Decoder
import kotlinx.serialization.encoding.Encoder
import org.jetbrains.exposed.sql.Table
import org.jetbrains.exposed.sql.javatime.date
import java.time.LocalDate
import java.util.*

/*
* LocalDateSerializer -> Funcion personalizada para serializar localdate.
* Se usa porque por defecto en kotlinx.serialization no son serializables.
* */
object LocalDateSerializer : KSerializer<LocalDate> {
    override val descriptor: SerialDescriptor = PrimitiveSerialDescriptor("LocalDate", PrimitiveKind.STRING)

    override fun serialize(encoder: Encoder, value: LocalDate) {
        encoder.encodeString(value.toString())
    }

    override fun deserialize(decoder: Decoder): LocalDate {
        return LocalDate.parse(decoder.decodeString())
    }
}

@Serializable
data class TesoroUsuario(
    val tu_id: Int,
    val tu_descubierto: Boolean,
    @Serializable(LocalDateSerializer::class) //Aplicando serializador personalizado.
    val tu_fecha: LocalDate,
    val tes_id: Int,
    val usu_id: Int
)
object TesoroUsuarios: Table(){
    val tu_id= integer("tu_id").autoIncrement()
    val tu_descubierto= bool("tu_descubierto")
    val tu_fecha= date("tu_fecha")
    val tes_id= integer("tes_id")
    val usu_id= integer("usu_id")

    override val primaryKey = PrimaryKey(tu_id)

}
