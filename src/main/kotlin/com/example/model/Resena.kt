 package com.example.model

import kotlinx.serialization.Serializable
import org.jetbrains.exposed.sql.Table

 @Serializable
data class Resena (
              val res_id: Int,
              val comentario: String,
              val puntuacion: Float,
              val foto: String,
              val tes_id: Int,
              val usu_id: Int,
              val user_name: String
              )


 object Resenas: Table() {
     val res_id = integer("res_id").autoIncrement()
     val comentario = text("res_comentario")
     val puntuacion = float("res_puntuacion")
     val foto = varchar("res_foto", 50)
     val tes_id = integer("tes_id")
     val usu_id = integer("usu_id")
     val username = varchar("usu_username", 50)
 }

val resenaStorage = mutableListOf<Resena>()