package com.example.model

import com.example.model.Jugadores.autoIncrement
import kotlinx.serialization.Serializable
import org.jetbrains.exposed.sql.Table



    @Serializable
    data class Admin(
        val usu_id: Int,
        val usu_username: String,
        val usu_password: String
    ){

    }

    object Admins: Table(){
        val usu_id= integer("usu_id").autoIncrement()
        val usu_username = varchar("usu_username",50)
        val usu_password = varchar("usu_userpassword",255)
        override val primaryKey = PrimaryKey(usu_id)
    }
