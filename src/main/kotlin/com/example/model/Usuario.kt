package com.example.model

import kotlinx.serialization.Serializable

@Serializable
abstract class Usuario(
    open val usu_id: Int,
    open val usu_username: String,
    open val usu_password: String
)
