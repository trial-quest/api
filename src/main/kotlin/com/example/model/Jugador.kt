package com.example.model

import com.example.model.TesoroUsuarios.autoIncrement
import com.example.model.TesoroUsuarios.references
import kotlinx.serialization.Serializable
import org.jetbrains.exposed.sql.Table
import org.jetbrains.exposed.sql.javatime.date

@Serializable
data class Jugador(
        val usu_foto: String,
        val usu_id: Int,
        val usu_username: String,
        val usu_password: String
)




object Jugadores: Table(){
        val usu_id= integer("usu_id").autoIncrement()
        val usu_username = varchar("usu_username",50)
        val usu_password = varchar("usu_userpassword",255)
        val usu_foto = varchar("usu_foto",50)

        override val primaryKey = PrimaryKey(usu_id)
}
