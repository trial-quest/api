package com.example.model

import kotlinx.serialization.Serializable
import org.jetbrains.exposed.sql.Table

@Serializable
data class Tesoro(
    val id: Int,
    val titulo: String,
    val descripcion: String,
    val latitud: Double,
    val longitud: Double,
    val valoracion: String,
    val foto_tesoro: String
)
object Tesoros: Table(){
    val id= integer("tes_id").autoIncrement()
    val titulo= varchar("tes_nombre", 30)
    val descripcion= text("tes_descripcion")
    val latitud= double("tes_latitud")
    val longitud= double("tes_longitud")
    val valoracion= varchar("tes_valoracion",20)
    val foto= varchar("tes_foto",50)
}
val tesorosStorage= mutableListOf<Tesoro>()



