package com.example.dao

import com.example.model.Resena

interface DAOResenas {
    suspend fun allResenas(): List<Resena>

    suspend fun addResena(comentario: String, puntuacion: Float, foto: String, tes_id: Int, usu_id: Int, username: String): Resena?

    suspend fun resenaById(id: Int): List<Resena>
}