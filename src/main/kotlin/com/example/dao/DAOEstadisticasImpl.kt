package com.example.dao

import com.example.dao.DatabaseFactory.dbQuery
import com.example.model.Estadistica
import com.example.model.Estadisticas
import kotlinx.coroutines.runBlocking
import org.jetbrains.exposed.sql.*
import org.jetbrains.exposed.sql.SqlExpressionBuilder.eq

class DAOEstadisticasImpl : DAOEstadisticas {

    private fun resultRowToEstadistica(row: ResultRow) = Estadistica(
        est_id = row[Estadisticas.est_id],
        usu_id = row[Estadisticas.usu_id],
        usu_porcentaje = row[Estadisticas.usu_porcentaje],
        usu_numTesorosEncontrados = row[Estadisticas.usu_numTesorosEncontrados]
    )
    override suspend fun allEstadisticas(): List<Estadistica> = dbQuery {
        Estadisticas.selectAll().map(::resultRowToEstadistica)
    }
    override suspend fun estadistica(est_id: Int): Estadistica? = dbQuery {
        Estadisticas
            .select{Estadisticas.est_id eq est_id}
            .map(::resultRowToEstadistica)
            .singleOrNull()
    }

    override suspend fun addNewEstadistica(
        usu_id: Int,
        usu_porcentaje: String,
        usu_numTesorosEncontrados: Int,
    ): Estadistica? = dbQuery {
        val insertStatement = Estadisticas.insert {
            it[Estadisticas.usu_id] = usu_id
            it[Estadisticas.usu_porcentaje] = usu_porcentaje
            it[Estadisticas.usu_numTesorosEncontrados] = usu_numTesorosEncontrados
        }
        insertStatement.resultedValues?.singleOrNull()?.let(::resultRowToEstadistica)
    }

    override suspend fun editEstadistica(
        est_id: Int,
        usu_id: Int,
        usu_porcentaje: String,
        usu_numTesorosEncontrados: Int
    ): Boolean = dbQuery {
        Estadisticas.update({Estadisticas.est_id eq est_id}){
            it[Estadisticas.usu_id] = usu_id
            it[Estadisticas.usu_porcentaje] = usu_porcentaje
            it[Estadisticas.usu_numTesorosEncontrados] = usu_numTesorosEncontrados
        } > 0
    }

    override suspend fun deleteEstadistica(est_id: Int): Boolean = dbQuery {
        Estadisticas.deleteWhere { Estadisticas.est_id eq est_id } > 0
    }
}
val daoEstadistica:DAOEstadisticas = DAOEstadisticasImpl().apply {
    runBlocking {
        if (allEstadisticas().isEmpty()){
            addNewEstadistica(1,"30%",1)
        }
    }
}