package com.example.dao

import com.example.dao.DatabaseFactory.dbQuery
import com.example.model.Tesoro
import com.example.model.Tesoros
import kotlinx.coroutines.runBlocking
import org.jetbrains.exposed.sql.*
import org.jetbrains.exposed.sql.SqlExpressionBuilder.eq


class DAOTesorosImpl: DAOTesoros {

    private fun resultRowToTesoro(row: ResultRow) = Tesoro(
        id = row[Tesoros.id],
        titulo= row[Tesoros.titulo],
        descripcion= row[Tesoros.descripcion],
        latitud = row[Tesoros.latitud],
        longitud= row[Tesoros.longitud],
        valoracion= row[Tesoros.valoracion],
        foto_tesoro = row[Tesoros.foto]
    )
    override suspend fun allTesoros(): List<Tesoro> = dbQuery{
        Tesoros.selectAll().map(::resultRowToTesoro)
    }


    override suspend fun tesoroName(titulo: String): List<Tesoro> = dbQuery {
        Tesoros
            .select {Tesoros.titulo eq titulo}
            .map(::resultRowToTesoro)
    }
    override suspend fun tesoroId(id: Int): Tesoro?= dbQuery {
        Tesoros
            .select {Tesoros.id eq id}
            .map(::resultRowToTesoro)
            .singleOrNull()
    }

    override suspend fun addNewTesoro(titulo: String, descripcion: String, latitud: Double, longitud: Double, valoracion: String, fotoTesoro: String): Tesoro? = dbQuery {
        val insertStatement = Tesoros.insert {
            it[Tesoros.titulo] = titulo
            it[Tesoros.descripcion] = descripcion
            it[Tesoros.latitud] = latitud
            it[Tesoros.longitud] = longitud
            it[Tesoros.valoracion] = valoracion
            it[foto] = fotoTesoro
        }
        insertStatement.resultedValues?.singleOrNull()?.let(::resultRowToTesoro)
    }

    override suspend fun editTesoro(id: Int, titulo: String, descripcion: String, latitud: Double, longitud: Double, valoracion: String, fotoTesoro: String): Boolean = dbQuery{
        Tesoros.update({Tesoros.id eq id}){
            it[Tesoros.titulo] = titulo
            it[Tesoros.descripcion] = descripcion
            it[Tesoros.latitud] = latitud
            it[Tesoros.longitud] = longitud
            it[Tesoros.valoracion] = valoracion
            it[foto] = fotoTesoro
        } > 0
    }

    override suspend fun deleteTesoro(id: Int): Boolean = dbQuery {
        Tesoros.deleteWhere { Tesoros.id eq id } > 0
    }
}

val dao: DAOTesoros = DAOTesorosImpl().apply {
    runBlocking {
        if(allTesoros().isEmpty()) {
            addNewTesoro("Tesoro de rellno", "No descripcion..", 3.3, 3.3, "8", "foto_prueba" )
        }
    }
}


