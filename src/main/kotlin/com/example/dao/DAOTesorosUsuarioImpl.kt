package com.example.dao

import com.example.dao.DatabaseFactory.dbQuery
import com.example.model.Tesoro
import com.example.model.TesoroUsuario
import com.example.model.TesoroUsuarios
import com.example.model.Tesoros
import kotlinx.coroutines.runBlocking
import org.jetbrains.exposed.sql.*
import org.jetbrains.exposed.sql.SqlExpressionBuilder.eq
import java.time.LocalDate

class DAOTesorosUsuarioImpl: DAOTesorosUsuario {

    private fun resultRowToTesoroUsuario(row: ResultRow) = TesoroUsuario(
        tu_id = row[TesoroUsuarios.tu_id],
        tu_descubierto = row[TesoroUsuarios.tu_descubierto],
        tu_fecha =  row[TesoroUsuarios.tu_fecha],
        tes_id =  row[TesoroUsuarios.tes_id],
        usu_id = row[TesoroUsuarios.usu_id]
    )


    override suspend fun tesorosEncontrados(user_id: Int): List<TesoroUsuario> = dbQuery {
        TesoroUsuarios
            .select{TesoroUsuarios.usu_id eq user_id and TesoroUsuarios.tu_descubierto.eq(true)}
            .map(::resultRowToTesoroUsuario)
    }


    override suspend fun addTesoroEncontrado(descubierto: Boolean, fecha: LocalDate, tesoroId: Int, usuarioId: Int): TesoroUsuario? = dbQuery {
        val insertStatement = TesoroUsuarios.insert {
            it[tu_descubierto] = descubierto
            it[tu_fecha] = fecha
            it[tes_id]= tesoroId
            it[usu_id]= usuarioId
        }
        insertStatement.resultedValues?.singleOrNull()?.let(::resultRowToTesoroUsuario)
    }

    override suspend fun deleteTesoro(id: Int, usu_id: Int): Boolean = dbQuery {
        Tesoros.deleteWhere { TesoroUsuarios.tes_id eq id and TesoroUsuarios.usu_id.eq(usu_id)} > 0
    }


}
val daoTesoroUsuario: DAOTesorosUsuario = DAOTesorosUsuarioImpl().apply {}