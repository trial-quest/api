package com.example.dao

import com.example.dao.DatabaseFactory.dbQuery
import com.example.model.Resena
import com.example.model.Resenas
import com.example.model.Tesoro
import com.example.model.Tesoros
import org.jetbrains.exposed.sql.ResultRow
import org.jetbrains.exposed.sql.insert
import org.jetbrains.exposed.sql.select
import org.jetbrains.exposed.sql.selectAll

class DAOResenasImpl: DAOResenas{
    private fun resultRowToResena(row: ResultRow) = Resena(
        res_id = row[Resenas.res_id],
        comentario= row[Resenas.comentario],
        puntuacion = row[Resenas.puntuacion],
        foto = row[Resenas.foto],
        tes_id= row[Resenas.tes_id],
        usu_id= row[Resenas.usu_id],
        user_name = row[Resenas.username]
    )
    override suspend fun allResenas(): List<Resena> = dbQuery {
        Resenas.selectAll().map(::resultRowToResena)
    }

    override suspend fun addResena(comentario: String, puntuacion: Float, foto: String, tes_id: Int, usu_id: Int, username: String): Resena?= dbQuery {
        val insertStatement = Resenas.insert {
            it[Resenas.comentario] = comentario
            it[Resenas.puntuacion] = puntuacion
            it[Resenas.foto] = foto
            it[Resenas.tes_id] = tes_id
            it[Resenas.usu_id] = usu_id
            it[Resenas.username] = username
        }
        insertStatement.resultedValues?.singleOrNull()?.let(::resultRowToResena)
    }

    override suspend fun resenaById(id: Int): List<Resena> = dbQuery{
        Resenas
            .select {Resenas.tes_id eq id}
            .map(::resultRowToResena)
    }

}
val daoResena: DAOResenas = DAOResenasImpl().apply {}