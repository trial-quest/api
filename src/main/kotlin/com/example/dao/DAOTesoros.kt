package com.example.dao

import com.example.model.Tesoro
import java.time.LocalDateTime

interface DAOTesoros {
    suspend fun allTesoros(): List<Tesoro>
    suspend fun tesoroName(name: String): List<Tesoro>
    suspend fun tesoroId(id: Int): Tesoro?
    suspend fun addNewTesoro(titulo: String, descripcion: String, latitud: Double, longitud: Double, valoracion: String, fotoTesoro: String):Tesoro?
    suspend fun editTesoro(id: Int, titulo: String, descripcion: String, latitud: Double, longitud: Double, valoracion: String, fotoTesoro: String): Boolean
    suspend fun deleteTesoro (id: Int): Boolean

}