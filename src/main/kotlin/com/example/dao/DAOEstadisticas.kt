package com.example.dao

import com.example.model.Estadistica
import com.example.model.Estadisticas

interface DAOEstadisticas {
    suspend fun allEstadisticas():List<Estadistica>

    suspend fun estadistica(est_id:Int):Estadistica?

    suspend fun addNewEstadistica(
        usu_id:Int,
        usu_porcentaje:String,
        usu_numTesorosEncontrados:Int
    ):Estadistica?

    suspend fun editEstadistica(
        est_id:Int,
        usu_id:Int,
        usu_porcentaje:String,
        usu_numTesorosEncontrados:Int
    ):Boolean

    suspend fun deleteEstadistica(est_id: Int):Boolean
}