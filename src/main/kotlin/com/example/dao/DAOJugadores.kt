package com.example.dao

import com.example.model.Jugador

interface DAOJugadores {
    suspend fun allJugadores(): List<Jugador>
    suspend fun jugador(usu_id: Int): Jugador?
    suspend fun addNewJugador(
        usu_username: String,
        usu_password: String,
    ): Jugador?
    suspend fun editJugador(usu_id: Int,
                            usu_username: String,
                            usu_password: String,
                            usu_foto: String
    ): Boolean
    suspend fun deleteJugador(usu_id: Int): Boolean
}