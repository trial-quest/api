package com.example.dao

import com.example.model.Tesoro
import com.example.model.TesoroUsuario
import java.time.LocalDate
import java.time.LocalDateTime

interface DAOTesorosUsuario {
    suspend fun tesorosEncontrados(user_id: Int): List<TesoroUsuario>
    suspend fun addTesoroEncontrado(descubierto: Boolean, fecha: LocalDate, tesoroId: Int, usuarioId: Int): TesoroUsuario?

    suspend fun deleteTesoro (id: Int, usu_id: Int): Boolean

}