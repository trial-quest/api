package com.example.dao

import com.example.dao.DatabaseFactory.dbQuery
import com.example.model.Jugador
import com.example.model.Jugadores
import kotlinx.coroutines.runBlocking
import org.jetbrains.exposed.sql.*
import org.jetbrains.exposed.sql.SqlExpressionBuilder.eq

class DAOJugadoresImpl : DAOJugadores {
    private fun resultRowToJugador(row: ResultRow) = Jugador(
        usu_id = row[Jugadores.usu_id],
        usu_username = row[Jugadores.usu_username],
        usu_password = row[Jugadores.usu_password],
        usu_foto = row[Jugadores.usu_foto]
    )
    override suspend fun allJugadores(): List<Jugador> = dbQuery {
        Jugadores.selectAll().map(::resultRowToJugador)
    }

    override suspend fun jugador(usu_id: Int): Jugador? = dbQuery {
        Jugadores
            .select{Jugadores.usu_id eq usu_id}
            .map(::resultRowToJugador)
            .singleOrNull()
    }

    override suspend fun addNewJugador(usu_username: String, usu_password: String): Jugador? = dbQuery {
        val insertStatement = Jugadores.insert {
            it[Jugadores.usu_username] = usu_username
            it[Jugadores.usu_password] = usu_password
            it[Jugadores.usu_foto] = "perfil.png"
        }
        insertStatement.resultedValues?.singleOrNull()?.let(::resultRowToJugador)
    }

    override suspend fun editJugador(usu_id: Int, usu_username: String, usu_password: String,usu_foto: String): Boolean = dbQuery {
        Jugadores.update({Jugadores.usu_id eq usu_id}){
            it[Jugadores.usu_username] = usu_username
            it[Jugadores.usu_password] = usu_password
            it[Jugadores.usu_foto] = usu_foto
        } > 0
    }

    override suspend fun deleteJugador(usu_id: Int): Boolean = dbQuery{
        Jugadores.deleteWhere {Jugadores.usu_id eq usu_id} > 0
    }


}
val daoJugador:DAOJugadores = DAOJugadoresImpl().apply {
    runBlocking {
        if (allJugadores().isEmpty()){
            addNewJugador("admin","123")
        }
    }
}