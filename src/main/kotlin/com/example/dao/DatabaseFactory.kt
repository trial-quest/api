package com.example.dao

import com.example.model.*
import kotlinx.coroutines.Dispatchers
import org.jetbrains.exposed.sql.Database
import org.jetbrains.exposed.sql.Schema
import org.jetbrains.exposed.sql.SchemaUtils
import org.jetbrains.exposed.sql.transactions.experimental.newSuspendedTransaction
import org.jetbrains.exposed.sql.transactions.transaction

object DatabaseFactory {
    fun init() {
        val driverClassName = "org.postgresql.Driver"
        val url = "jdbc:postgresql://rogue.db.elephantsql.com/vhwuuclo"
        val user = "vhwuuclo"
        val password = "XK4HZbD7mrCKzuidzgopdfvGVsPUUYVv"
        val database = Database.connect(url, driverClassName, user,password)
        transaction(database) {
            SchemaUtils.create(Tesoros)
            SchemaUtils.create(Admins)
            SchemaUtils.create(Jugadores)
            SchemaUtils.create(Resenas)
            SchemaUtils.create(Estadisticas)
            SchemaUtils.create(TesoroUsuarios)
        }
    }

    suspend fun <T> dbQuery(block: suspend () -> T): T {
        return newSuspendedTransaction(Dispatchers.IO) {
            block()
        }
    }

}

