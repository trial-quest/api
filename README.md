# TRAIL QUEST API

    Este repositorio contiene la API de nuestro proyecto Trail Quest. 
    
## Índice

1. Descripción del proyecto
2. Programas necesarios
3. Instalación
4. Tecnologias utilizadas
5. Cómo utilizarlo
6. Autores


##  Descripción del proyecto :confused: <a name="1"></a>

    La API de Trail Quest contiene los datos e imagenes que permite
    a nuestra aplicacion móvil y web obtener los datos necesario
    para garantizar al usuario una interfaz correcta. 

    Es una API creada desde cero la cual se comunica con una base de
    datos en la nube para recibir los datos y trasladarlos al cliente.
    Contiene imagenes que luego carga en las aplicaciones y todas
    sus rutas estan protegidas con la tecnología de encriptación Digest
    Authentication.


## Programas necesarios  :eyes:

- IntelliJ IDEA Ultimate
- Git

## Instalación :hammer:

- `Paso 1`: Ve a la pagina: https://www.jetbrains.com/idea/
- `Paso 2`: Descarga IntelliJ IDEA Ultimate en la version acorde a tu sistema operativo
- `Paso 3`: Instalar Git: https://git-scm.com/downloads
- `Paso 4`: $ git clone https://gitlab.com/trial-quest/api.git
- `Paso 5`: Abrir el proyecto con IntelliJ IDEA Ultimate
- `Paso 6`: Una vez abierto, selecciona el archivo "Application"
- `Paso 7`: Arriba a la derecha verás un icono de play (:arrow_forward:)
- `Paso 8`: Presionalo y el servidor estará en funcionamiento.

## Tecnologias utilizadas :rocket:

- Kotlin
- Ktor
- Git

## Cómo utilizarlo :question:

    Se trata de un servidor en local. Siguiendo los pasos anteriormente
    mencionados, desde la aplicacion que desees obtener datos deberías
    poder acceder a ellos siempre que tengas la IP correcta.


## Autores :wave:

    ARNAU BADENAS VIVES
    CARLOS TARRIAS DÍAZ
    ANA GÓMEZ PASTOR
    ALEJANDRO TORRES LEMOS
    DAVID MORENO FERNÁNDEZ
    JORDI ROBLES PARERA
    DIEGO ARTEAGA QUEVEDO